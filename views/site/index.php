<?php

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $statement \app\models\Statement */

$this->title = 'Statement';

?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>

        <?php $form = ActiveForm::begin([
            'options' => [
                'class' => 'form',
                'enctype' => 'multipart/form-data',
            ]
        ]) ?>
        <div class="form-row">
            <?= $form->field($model, 'parseFile')->fileInput()->label(false) ?>
            <?= \yii\helpers\Html::submitButton('Load File', ['class' => 'btn btn-xs btn-success']) ?>
        </div>
        <?php ActiveForm::end() ?>
    </div>

</div>

<div class="body-content">

    <div class="row">
        <div class="col-lg-12">
            <h2>График</h2>
            <?= $this->render("@app/views/layouts/_chart", ['statement' => $statement]) ?>
        </div>
    </div>

</div>
