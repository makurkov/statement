<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 16.08.2019
 * Time: 20:56
 */

use onmotion\apexcharts\ApexchartsWidget; //Выбрал его, т.к. есть функции зума и выглядит приемлемо

/* @var $statement \app\models\Statement */

?>

<?
if (!empty($statement->deals)) {

    echo ApexchartsWidget::widget([
        'type' => 'line',
        'height' => '400',
        'chartOptions' => [
            'chart' => [
                'toolbar' => [
                    'show' => true,
                    'autoSelected' => 'zoom'
                ],
            ],
            'xaxis' => [
                'type' => 'datetime',
                'labels' => [
                    'datetimeFormatter' => [
                        'year' => 'yyyy',
                        'month' => 'MMM \'yy',
                        'day' => 'dd MMM',
                        'hour' => 'HH:mm:ss'
                    ]
                ],
            ],

            'title' => [
                'text' => 'Profit chart',
                'align' => 'left'
            ],
            'grid' => [
                'row' => [
                    'colors' => ['#f3f3f3', 'transparent'],
                    'opacity' => 0.5
                ]
            ],

        ],
        'series' => [
            [
                'name' => "Profit",
                'data' => $statement->deals
            ]
        ],


    ]);
} else {
    echo "<p>Profit data not found</p>";
}
?>
