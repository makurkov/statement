<?php

namespace app\controllers;

use app\models\Statement;
use app\models\UploadForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new UploadForm();
        $statement = new Statement();

        if (Yii::$app->request->isPost) {
            $model->parseFile = UploadedFile::getInstance($model, 'parseFile');
            if ($model->upload()) {
                $fileName = Yii::getAlias('@app') . '\web\uploads\\' . $model->parseFile->baseName . '.' . $model->parseFile->extension;
               $statement->getDeals($fileName);

            }
        }

        return $this->render('index', ['model' => $model, 'statement' => $statement]);
    }
}
