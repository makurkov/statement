<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 14.08.2019
 * Time: 22:38
 */

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use darkdrim\simplehtmldom\SimpleHTMLDom as SHD;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $parseFile;

    public function rules()
    {
        return [
            [['parseFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'html'],
            [['parseFile'], 'validateTable']
        ];
    }

    // Проверка содержимого
    public function validateTable($attribute, $params)
    {
        $parseFile = file_get_contents($this->$attribute->tempName);

        $html = SHD::str_get_html($parseFile);
        if (!Statement::checkFormat($html)) {
            $this->addError($attribute, 'Date format error');
        }
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->parseFile->saveAs('uploads/' . $this->parseFile->baseName . '.' . $this->parseFile->extension);
            return true;
        } else {
            return false;
        }
    }
}