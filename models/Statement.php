<?php

namespace app\models;

use darkdrim\simplehtmldom\simple_html_dom;
use Yii;
use darkdrim\simplehtmldom\SimpleHTMLDom as SHD;
use yii\base\Model;

class Statement extends Model
{
    const COLUMNS_COUNT = 14;

    public $deals;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->getDeals();
    }

    /**
     * @param null $parseFilePath
     * @return false|mixed|string
     */
    public function getDeals($parseFilePath = null)
    {
        $result = [];

        if (empty($parseFilePath)) {
            $parseFilePath = Yii::getAlias('@app') . '\web\statement1.html';
        }

        $parseFile = file_get_contents($parseFilePath);

        $html = SHD::str_get_html($parseFile);
        if ($html && self::checkFormat($html)) {
            // Исходя из файла
            $openTimeCol = 1; // Номер колонки с временм
            $typeCol = 2; // Номер колонки типа сделки
            $execTypes = ['buy', 'balance']; // Типы сделки влияющие на изменение

            $table = $html->find('table', 0);
            $curBalance = 0;

            foreach ($table->children() as $row) {
                $type = $row->children($typeCol)->innertext;

                if (in_array($type, $execTypes)) {
                    $curBalance = $curBalance + (double)str_replace(' ', '', $row->last_child()->innertext);
                    $date = $row->children($openTimeCol)->innertext;
                    $result[] = ['x' => $date, 'y' => round($curBalance, 2)];
                }
            }
        }
        $this->deals = $result;
        return $this->deals;
    }

    /**
     * Проверка: имеется ли в файле валидная для обработчика таблица
     * @param simple_html_dom $html
     * @return bool
     */
    public static function checkFormat($html)
    {
        $res = false;

        if (count($html->find('table', 0)->find('tr', 2)->children()) == self::COLUMNS_COUNT) {
            $res = true;
        }

        return $res;
    }
}
